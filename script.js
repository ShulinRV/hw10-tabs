//finder,

function setTab() {
  let listTabsTitle = document.querySelectorAll(".tabs-title");
  let listTabsContent = document.querySelectorAll(".content");
  let attribute;

  listTabsTitle.forEach((item) => {
    item.addEventListener("click", addClick);
  });

  function addClick() {
    listTabsTitle.forEach((item) => {
      item.classList.remove("active");
    });
    this.classList.add("active");
    attribute = this.dataset.name;
    containContent(attribute);
  }

  function containContent(attr) {
    listTabsContent.forEach((item) => {
      item.classList.contains(attr)
        ? item.classList.add("active-content")
        : item.classList.remove("active-content");
    });
  }
}
setTab();
